//
//  PoseCNN_Multi_15_ModelEvaluationService.swift
//  Spine
//
//  Created by Александр Пахомов on 13.05.2020.
//  Copyright © 2020 Александр Пахомов. All rights reserved.
//

import UIKit
import PostureVision


final class PoseCNN_Multi_15_ModelEvaluationService: ModelEvaluationServiceProtocol {
    private let modelEvaluationService: ModelEvaluationServiceProtocol

    init() {
        let model = PoseCNN_Multi_15().model
        let detectorConf = PoseCNN_Multi_15.pointsDetectionConfiguration
        let transformerConf = PoseCNN_Multi_15.heatmapTransformationConfiguration
        let connectorConf = PoseCNN_Multi_15.pointsConnectionConfiguration
        let translatorConf = PoseCNN_Multi_15.pointsTranslatorConfiguration
        let painterConf = PoseCNN_Multi_15.imagePainterConfiguration

        let detector = PVPointsDetector(model: model, configuration: detectorConf)
        let transformer = PVHeatmapToPointsTransformer(configuration: transformerConf)
        let connector = PVPointsConnector(configuration: connectorConf)
        let translator = PVPointsTranslator(configuration: translatorConf)
        let painter = PVConnectionsPainter(configuration: painterConf)

        modelEvaluationService = ModelEvaluationService(
            detector: detector,
            transformer: transformer,
            connector: connector,
            translator: translator,
            painter: painter
        )
    }

    func evaluate(on image: UIImage, completion: @escaping Completion) {
        modelEvaluationService.evaluate(on: image, completion: completion)
    }
}

