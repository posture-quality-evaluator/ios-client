//
//  MetricsEvaluationService.swift
//  Spine
//
//  Created by Александр Пахомов on 09.05.2020.
//  Copyright © 2020 Александр Пахомов. All rights reserved.
//

import Foundation
import PostureVision


protocol MetricsEvaluationServiceProtocol: class {
    func evaluate(on mappedPoints: [PVPointType: [PVPointLocation]]) -> MetricsEvaluationResult
}

struct MetricsEvaluationResult {
    var superDecision: PVSuperMetricDecisionProtocol
}

final class MetricsEvaluationService: MetricsEvaluationServiceProtocol {

    // MARK: - API

    init(metrics: [PVMetricProtocol], superMetric: PVSuperMetricProtocol) {
        self.metrics = metrics
        self.superMetric = superMetric
    }

    func evaluate(on mappedPoints: [PVPointType: [PVPointLocation]]) -> MetricsEvaluationResult {
        let mappedPoints = mappedPoints.mapValues { $0.first! }
        let measurement = PVMeasurement(points: mappedPoints)
        return evaluate(metrics: metrics, on: measurement, using: superMetric)
    }

    // MARK: - Private

    private let metrics: [PVMetricProtocol]
    private let superMetric: PVSuperMetricProtocol

    private func evaluate(metrics: [PVMetricProtocol], on measurement: PVMeasurement, using superMetric: PVSuperMetricProtocol) -> MetricsEvaluationResult {
        let results = metrics.map { $0.compute(on: measurement) }
        let decision = superMetric.makeSuperDecision(on: results)
        return MetricsEvaluationResult(superDecision: decision)
    }
}
