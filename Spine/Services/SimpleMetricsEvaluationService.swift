//
//  SimpleMetricsEvaluationService.swift
//  Spine
//
//  Created by Александр Пахомов on 09.05.2020.
//  Copyright © 2020 Александр Пахомов. All rights reserved.
//

import Foundation
import PostureVision


final class SimpleMetricsEvaluationService: MetricsEvaluationServiceProtocol {

    private let service: MetricsEvaluationServiceProtocol

    init() {
        let metrics: [PVMetricProtocol] = [
            RandomMetric()
        ]

        let superMetric: PVSuperMetricProtocol = VotingSuperMetric()

        self.service = MetricsEvaluationService(metrics: metrics, superMetric: superMetric)
    }

    func evaluate(on mappedPoints: [PVPointType : [PVPointLocation]]) -> MetricsEvaluationResult {
        return service.evaluate(on: mappedPoints)
    }
}
