//
//  PoseMNV2_Single_14_ModelEvaluationService.swift
//  Spine
//
//  Created by Александр Пахомов on 09.05.2020.
//  Copyright © 2020 Александр Пахомов. All rights reserved.
//

import UIKit
import PostureVision


final class PoseMNV2_Single_14_ModelEvaluationService: ModelEvaluationServiceProtocol {
    private let modelEvaluationService: ModelEvaluationServiceProtocol

    init() {
        let model = PoseMNV2_Single_14().model
        let detectorConf = PoseMNV2_Single_14.pointsDetectionConfiguration
        let transformerConf = PoseMNV2_Single_14.heatmapTransformationConfiguration
        let connectorConf = PoseMNV2_Single_14.pointsConnectionConfiguration
        let translatorConf = PoseMNV2_Single_14.pointsTranslatorConfiguration
        let painterConf = PoseMNV2_Single_14.imagePainterConfiguration

        let detector = PVPointsDetector(model: model, configuration: detectorConf)
        let transformer = PVHeatmapToPointsTransformer(configuration: transformerConf)
        let connector = PVPointsConnector(configuration: connectorConf)
        let translator = PVPointsTranslator(configuration: translatorConf)
        let painter = PVConnectionsPainter(configuration: painterConf)

        modelEvaluationService = ModelEvaluationService(
            detector: detector,
            transformer: transformer,
            connector: connector,
            translator: translator,
            painter: painter
        )
    }

    func evaluate(on image: UIImage, completion: @escaping Completion) {
        modelEvaluationService.evaluate(on: image, completion: completion)
    }
}
