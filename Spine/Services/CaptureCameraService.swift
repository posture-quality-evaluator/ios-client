//
//  CaptureCameraService.swift
//  Spine
//
//  Created by Александр Пахомов on 08.05.2020.
//  Copyright © 2020 Александр Пахомов. All rights reserved.
//

import UIKit
import AVFoundation

protocol CapturePhotoServiceProtocol: class {
    var session: AVCaptureSession { get }
    var isCapturingSessionRunning: Bool { get set }

    func capturePhoto(completion: @escaping (Result<UIImage, Error>) -> Void)
}

class CapturePhotoService: NSObject, CapturePhotoServiceProtocol {

    // MARK: - API

    override init() {
        super.init()
        setupCamera()
    }

    let session = AVCaptureSession()

    var isCapturingSessionRunning = false {
        didSet {
            guard oldValue != isCapturingSessionRunning else { return }

            if isCapturingSessionRunning {
                session.startRunning()
            } else {
                session.stopRunning()
            }
        }
    }

    func capturePhoto(completion: @escaping (Result<UIImage, Error>) -> Void) {
        guard capturingCompletion == nil else {
            logger.assertionFailure("Trying to capture photo when capturing is already in progress")
            return
        }

        capturingCompletion = completion

        let settings = AVCapturePhotoSettings()

        capturePhotoOutput.capturePhoto(with: settings, delegate: self)
    }

    // MARK: - Private

    private var capturingCompletion: ((Result<UIImage, Error>) -> Void)?
    private var capturePhotoOutput = AVCapturePhotoOutput()

    private func setupCamera() {
        guard let camera = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back) else {
            logger.warning("Couldn't get Camera device")
            return
        }

        var input: AVCaptureDeviceInput!
        do {
            input = try AVCaptureDeviceInput(device: camera)
        } catch {
            logger.warning("Couldn't create camera input from camera with error: \(error.debugDescription)")
            return
        }

        guard session.canAddInput(input) else {
            logger.warning("Couldn't add input into session")
            return
        }

        guard session.canAddOutput(capturePhotoOutput) else {
            logger.warning("Couldn't add output into session")
            return
        }

        capturePhotoOutput.isHighResolutionCaptureEnabled = true
        capturePhotoOutput.connection(with: .video)?.videoOrientation = .portrait

        session.sessionPreset = .photo
        session.addInput(input)
        session.addOutput(capturePhotoOutput)

        assert(!session.isRunning)
    }

    private func callCompletion(with result: Result<UIImage, Error>) {
        assert(capturingCompletion != nil)
        capturingCompletion?(result)
        capturingCompletion = nil
    }

    private func postprocessImage(_ image: AVCapturePhoto) throws -> UIImage {
        guard let cgImage = image.cgImageRepresentation()?.takeUnretainedValue() else {
            throw ErrorImpl("Unknown error extracting image from photo capture")
        }

        let ciImage = CIImage(cgImage: cgImage)
            .oriented(forExifOrientation: 6)
            .transformed(by: CGAffineTransform(scaleX: -1, y: 1))

        let proportion: CGFloat = 4.0 / 3
        guard let finalImage = UIImage.convert(from: ciImage)?.cropAspectFit(with: proportion) else {
            throw ErrorImpl("Convert CIImage to UIImage")
        }

        return finalImage
    }
}

extension CapturePhotoService: AVCapturePhotoCaptureDelegate {
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        if let error = error {
            logger.error("Error capturing photo: \(error.debugDescription)")
            callCompletion(with: .failure(error))
            return
        }

        do {
            let image = try postprocessImage(photo)
            callCompletion(with: .success(image))
        } catch {
            logger.error(error)
            callCompletion(with: .failure(error))
        }
    }
}
