//
//  ModelEvaluationService.swift
//  Spine
//
//  Created by Александр Пахомов on 08.05.2020.
//  Copyright © 2020 Александр Пахомов. All rights reserved.
//

import UIKit
import CoreML
import PostureVision

protocol ModelEvaluationServiceProtocol: class {
    typealias Completion = (Result<ModelEvaluationResult, Error>) -> Void

    func evaluate(on image: UIImage, completion: @escaping Completion)
}

struct ModelEvaluationResult {
    var paintedImage: UIImage
    var mappedPoints: [PVPointType: [PVPointLocation]]
    var connections: [PVLine]
}

class ModelEvaluationService: ModelEvaluationServiceProtocol {

    // MARK: - API

    typealias Detector = PVPointsDetectorProtocol
    typealias Transformer = PVHeatmapToPointsTransformerProtocol
    typealias Connector = PVPointsConnectorProtocol
    typealias Translator = PVPointsTranslatorProtocol
    typealias Painter = PVConnectionsPainterProtocol

    init(detector: Detector, transformer: Transformer, connector: Connector, translator: Translator, painter: Painter) {
        self.detector = detector
        self.transformer = transformer
        self.connector = connector
        self.translator = translator
        self.painter = painter
    }

    func evaluate(on image: UIImage, completion: @escaping Completion) {
        do {
            try detector.detectPoints(on: image, usingCompletionQueue: .main) { [weak self] result in
                self?.handleDetectionResult(result, on: image, completion: completion)
            }
        } catch {
            let error = ErrorImpl("Error trying to start detecting points: \(error.debugDescription)")
            logger.error(error)
            completion(.failure(error))
        }
    }

    // MARK: - Private

    private let detector: Detector
    private let transformer: Transformer
    private let connector: Connector
    private let translator: Translator
    private let painter: Painter

    private func handleDetectionResult(_ result: Result<PVPointsDetectionResult, PVError>, on image: UIImage, completion: @escaping Completion) {
        switch result {
        case .failure(let error):
            let error = ErrorImpl("Error detecting points on image: \(error.debugDescription)")
            logger.error(error.debugDescription)
            completion(.failure(error))

        case .success(let result):
            let statistics = result.statistics
            let heatmaps = result.heatmaps
            logger.info("Points detection statistics: \(statistics)")

            transformer.transform(heatmaps: heatmaps, originalImageSize: image.size, usingCompletionQueue: .main) { [weak self] result in
                self?.handleHeatmapsTransformationResult(result, on: image, heatmaps: heatmaps, completion: completion)
            }
        }
    }

    private func handleHeatmapsTransformationResult(_ result: Result<PVHeatmapToPointsTransformationResult, PVError>, on image: UIImage, heatmaps: MLMultiArray, completion: @escaping Completion) {
        switch result {
        case .failure(let error):
            let error = ErrorImpl("Error transforming heatmaps from points: \(error.debugDescription)")
            logger.error(error.debugDescription)
            completion(.failure(error))

        case .success(let result):
            let statistics = result.statistics
            let mappedPoints = result.mappedPoints
            logger.info("Heatmaps transformation statistics: \(statistics)")

            let connections = connector.connect(mappedPoints: mappedPoints, heatmaps: heatmaps, originalImageSize: image.size)
            let translatedConnections = translator.translate(connections, usingOriginalImageSize: image.size)
            handleMappedPointsConnectionResult(translatedConnections, mappedPoints: mappedPoints, on: image, completion: completion)

        }
    }

    private func handleMappedPointsConnectionResult(_ result: [PVLine], mappedPoints: [PVPointType: [PVPointLocation]], on image: UIImage, completion: @escaping Completion) {
        painter.paint(connections: result, over: image, usingCompletionQueue: .main) { paintedImage in
            let result = ModelEvaluationResult(
                paintedImage: paintedImage,
                mappedPoints: mappedPoints,
                connections: result
            )

            completion(.success(result))
        }
    }
}
