//
//  Logger.swift
//  Spine
//
//  Created by Александр Пахомов on 08.05.2020.
//  Copyright © 2020 Александр Пахомов. All rights reserved.
//

import SwiftyBeaver

let logger = SwiftyBeaver.self

extension SwiftyBeaver {
    class func assertionFailure(_ message: Any, _ file: String = #file, _ function: String = #function, line: Int = #line, context: Any? = nil) {
        custom(level: .error, message: message, file: file, function: function, line: line, context: context)
        assertionFailure(message)
    }

    class func assert(condition: Bool, _ message: @autoclosure () -> Any? = nil, _ file: String = #file, _ function: String = #function, line: Int = #line, context: Any? = nil) {
        guard !condition else { return }
        assertionFailure(message() ?? "", file, function, line: line, context: context)
    }
}
