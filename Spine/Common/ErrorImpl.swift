//
//  ErrorImpl.swift
//  Spine
//
//  Created by Александр Пахомов on 07.05.2020.
//  Copyright © 2020 Александр Пахомов. All rights reserved.
//

import Foundation

class ErrorImpl: Error {
    let describtion: String

    convenience init(_ description: String, _ object: Any, filename: String = #file, function: String = #function) {
        self.init("\(description): \(String(describing: object))", filename: filename, function: function)
    }

    init(_ describtion: String, filename: String = #file, function: String = #function) {
        self.describtion = "\(ErrorImpl.sourceFileName(filePath: filename)): \(function) -> \(describtion)"
    }

    private class func sourceFileName(filePath: String) -> String {
        let components = filePath.components(separatedBy: "/")
        return components.last ?? ""
    }
}

extension ErrorImpl: LocalizedError {
    var errorDescription: String? {
        return describtion
    }
}
