//
//  Asset.swift
//  Spine
//
//  Created by Александр Пахомов on 08.05.2020.
//  Copyright © 2020 Александр Пахомов. All rights reserved.
//

import UIKit

enum Asset {
    enum Color {
        static let main: UIColor = UIColor(named: "Main")!
        static let grayGradientTop: UIColor = UIColor(named: "GrayGradientTop")!
        static let grayGradientBottom: UIColor = UIColor(named: "GrayGradientBottom")!
    }
}
