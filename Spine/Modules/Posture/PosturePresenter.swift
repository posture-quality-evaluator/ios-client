//
//  PosturePresenter.swift
//  Spine
//
//  Created by Александр Пахомов on 08.05.2020.
//  Copyright © 2020 Александр Пахомов. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import AVFoundation


protocol PosturePresenterProtocol: class {
    func configureView()

    func pickImageButtonDidTap()
    func shootButtonDidTap()
    func openSettingsButtonDidTap()
    func saveResultsImageButtonDidTap()
    func repeatButtonDidTap()

    var oResultTitle: Observable<String?> { get }
    var oHasActivity: Observable<Bool> { get }
    var oIsScrollEnabled: Observable<Bool> { get }
    var oImage: Observable<UIImage?> { get }
    var oCameraCaptureSession: Observable<AVCaptureSession?> { get }
    var oIsPhotoDashboardHidden: Observable<Bool> { get }
    var oIsResultsDashboardHidden: Observable<Bool> { get }
}

class PosturePresenter: NSObject, PosturePresenterProtocol {

    // MARK: - API

    // MARK: Variables

    weak var view: PostureViewProtocol!
    var interactor: PostureInteractorProtocol!
    var router: PostureRouterProtocol!

    // MARK: Observable

    var oResultTitle: Observable<String?> {
        resultTitle.asObservable()
    }

    var oHasActivity: Observable<Bool> {
        hasActivity.asObservable()
    }

    var oIsScrollEnabled: Observable<Bool> {
        isScrollable.asObservable()
    }

    var oImage: Observable<UIImage?> {
        image.asObservable()
    }

    var oCameraCaptureSession: Observable<AVCaptureSession?> {
        cameraCaptureSession.asObservable()
    }

    var oIsPhotoDashboardHidden: Observable<Bool> {
        isPhotoDashboardHidden.asObservable()
    }

    var oIsResultsDashboardHidden: Observable<Bool> {
        isResultsDashboardHidden.asObservable()
    }

    // MARK: Methods

    func configureView() {
        resultTitle = BehaviorRelay<String?>(value: nil)
        hasActivity = .init(value: false)
        isScrollable = .init(value: false)
        image = .init(value: nil)
        cameraCaptureSession = .init(value: nil)
        isPhotoDashboardHidden = .init(value: false)
        isResultsDashboardHidden = .init(value: true)

        setupObservations()
    }

    func pickImageButtonDidTap() {
        router.pickImage(with: self, style: .popover, animated: true, completion: nil)
    }

    func shootButtonDidTap() {
        interactor.evaluateWithPhotoCapture()
    }

    func openSettingsButtonDidTap() {
        router.openSettings()
    }

    func saveResultsImageButtonDidTap() {

    }

    func repeatButtonDidTap() {
        interactor.clearResult()
    }

    // MARK: - Private

    // MARK: Variables

    private var resultTitle: BehaviorRelay<String?>!
    private var hasActivity: BehaviorRelay<Bool>!
    private var isScrollable: BehaviorRelay<Bool>!
    private var image: BehaviorRelay<UIImage?>!
    private var cameraCaptureSession: BehaviorRelay<AVCaptureSession?>!
    private var isPhotoDashboardHidden: BehaviorRelay<Bool>!
    private var isResultsDashboardHidden: BehaviorRelay<Bool>!

    private let bag = DisposeBag()

    // MARK: Methods

    private func setupObservations() {
        interactor.oCapturePhotoSession.bind(to: cameraCaptureSession).disposed(by: bag)

        let hasAnyActivity = Observable.combineLatest([interactor.oEvaluationInProgress, interactor.oPhotoCapturingInProgress])
            .debounce(.milliseconds(50), scheduler: MainScheduler.instance)
            .map({ $0.contains(true)})
            .distinctUntilChanged()

        let hasResult = interactor.oEvaluationResult
            .map { $0 != nil }
            .distinctUntilChanged()

        hasAnyActivity.bind(to: hasActivity).disposed(by: bag)

        interactor.oEvaluationResult
            .subscribe(onNext: { [weak self] newResult in
                guard let self = self else { return }

                if let result = newResult {
                    self.image.accept(result.image)

                    if result.isGood {
                        self.resultTitle.accept("Your posture is OK!")
                    } else {
                        self.resultTitle.accept("Your posture is not OK")
                    }
                } else {
                    self.image.accept(nil)
                    self.resultTitle.accept(nil)
                }
            }
        ).disposed(by: bag)

        hasResult.bind(to: isScrollable).disposed(by: bag)

        Observable.combineLatest(hasAnyActivity, hasResult).map { $0.0 || $0.1 }.bind(to: isPhotoDashboardHidden).disposed(by: bag)
        Observable.combineLatest(hasAnyActivity, hasResult).map { $0.0 || !$0.1 }.bind(to: isResultsDashboardHidden).disposed(by: bag)
    }
}

extension PosturePresenter: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        router.dismissImagePicker(animated: true, completion: nil)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        router.dismissImagePicker(animated: true, completion: nil)

        if let image = info[.originalImage] as? UIImage {
            self.image.accept(image)
            interactor.evaluate(on: image)
        }
    }
}

extension Observable where Element == Bool {
    func inverse() -> Observable<Bool> {
        return map { !$0 }
    }
}
