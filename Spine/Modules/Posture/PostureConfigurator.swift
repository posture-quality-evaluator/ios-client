//
//  PostureConfigurator.swift
//  Spine
//
//  Created by Александр Пахомов on 08.05.2020.
//  Copyright © 2020 Александр Пахомов. All rights reserved.
//

import Foundation


protocol PostureConfiguratorProtocol: class {
    func configure(with viewController: PostureViewController)
}

class PostureConfigurator: PostureConfiguratorProtocol {

    // MARK: - API

    func configure(with viewController: PostureViewController) {
        let presenter = PosturePresenter()
        let interactor = PostureInteractor()
        let router = PostureRouter()

        viewController.presenter = presenter
        presenter.interactor = interactor
        presenter.router = router
        router.viewController = viewController
    }
}
