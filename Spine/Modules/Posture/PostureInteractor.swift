//
//  PostureInteractor.swift
//  Spine
//
//  Created by Александр Пахомов on 08.05.2020.
//  Copyright © 2020 Александр Пахомов. All rights reserved.
//

import UIKit
import AVFoundation
import RxSwift
import RxCocoa


protocol PostureInteractorProtocol: class {
    func evaluate(on image: UIImage)
    func evaluateWithPhotoCapture()
    func clearResult()

    var oCapturePhotoSession: Observable<AVCaptureSession?> { get }
    var oAuthorizationStatus: Observable<AVAuthorizationStatus> { get }
    var oEvaluationInProgress: Observable<Bool> { get }
    var oPhotoCapturingInProgress: Observable<Bool> { get }
    var oEvaluationResult: Observable<PostureInteractorEvaluationResult?> { get }
}

struct PostureInteractorEvaluationResult {
    var isGood: Bool
    var image: UIImage
}

class PostureInteractor: NSObject, PostureInteractorProtocol {

    // MARK: - API

    override init() {
        self.capturePhotoSession = .init(value: nil)
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: .video)
        self.authorizationStatus = .init(value: cameraAuthorizationStatus)
        self.evaluationInProgress = .init(value: false)
        self.photoCapturingInProgress = .init(value: false)
        self.evaluationResult = .init(value: nil)

        self.capturePhotoService = CapturePhotoService()

        self.modelEvaluationService = PoseCNN_Multi_15_ModelEvaluationService()

        self.metricsEvaluationService = SimpleMetricsEvaluationService()

        super.init()

        capturePhotoSession.accept(capturePhotoService.session)
        capturePhotoService.isCapturingSessionRunning = true
    }

    func evaluate(on image: UIImage) {
        assert(!evaluationInProgress.value)
        assert(!photoCapturingInProgress.value)

        capturePhotoService.isCapturingSessionRunning = false
        evaluationInProgress.accept(true)

        modelEvaluationService.evaluate(on: image) { [weak self] result in
            self?.handleModelEvaluationResult(result)
        }
    }

    func evaluateWithPhotoCapture() {
        assert(!evaluationInProgress.value)
        assert(!photoCapturingInProgress.value)

        self.photoCapturingInProgress.accept(true)

        capturePhotoService.capturePhoto { [weak self] result in
            self?.handleCapturePhotoResult(result)
        }
    }

    func clearResult() {
        evaluationResult.accept(nil)
        capturePhotoService.isCapturingSessionRunning = true
    }

    var oCapturePhotoSession: Observable<AVCaptureSession?> {
        capturePhotoSession.asObservable()
    }

    var oAuthorizationStatus: Observable<AVAuthorizationStatus> {
        authorizationStatus.asObservable()
    }

    var oEvaluationInProgress: Observable<Bool> {
        evaluationInProgress.asObservable()
    }

    var oPhotoCapturingInProgress: Observable<Bool> {
        photoCapturingInProgress.asObservable()
    }

    var oEvaluationResult: Observable<PostureInteractorEvaluationResult?> {
        evaluationResult.asObservable()
    }

    // MARK: - Private

    private var capturePhotoSession: BehaviorRelay<AVCaptureSession?>
    private var authorizationStatus: BehaviorRelay<AVAuthorizationStatus>
    private var evaluationInProgress: BehaviorRelay<Bool>
    private var photoCapturingInProgress: BehaviorRelay<Bool>
    private var evaluationResult: BehaviorRelay<PostureInteractorEvaluationResult?>

    private let capturePhotoService: CapturePhotoServiceProtocol
    private let modelEvaluationService: ModelEvaluationServiceProtocol
    private let metricsEvaluationService: MetricsEvaluationServiceProtocol

    private func handleCapturePhotoResult(_ result: Result<UIImage, Error>) {
        photoCapturingInProgress.accept(false)

        switch result {
        case .success(let image):
            evaluate(on: image)
        case .failure(let error):
            logger.error("Error evaluating with capturing: \(error.debugDescription)")
        }
    }

    private func handleModelEvaluationResult(_ result: Result<ModelEvaluationResult, Error>) {
        evaluationInProgress.accept(false)

        switch result {
        case .failure(let error):
            logger.error("Error evaluating model: \(error.debugDescription)")
            evaluationResult.accept(nil)

        case .success(let result):
            let mappedPoints = result.mappedPoints
            let metricsResult = metricsEvaluationService.evaluate(on: mappedPoints)
            let superDecision = metricsResult.superDecision

            let result = PostureInteractorEvaluationResult(
                isGood: superDecision.superDecision,
                image: result.paintedImage
            )

            evaluationResult.accept(result)

        }
    }
}
