//
//  PostureRouter.swift
//  Spine
//
//  Created by Александр Пахомов on 08.05.2020.
//  Copyright © 2020 Александр Пахомов. All rights reserved.
//

import UIKit


protocol PostureRouterProtocol: class {
    func openSettings()
    func pickImage(with delegate: UIImagePickerControllerDelegate & UINavigationControllerDelegate, style: UIModalPresentationStyle, animated: Bool, completion: (() -> Void)?)
    func dismissImagePicker(animated: Bool, completion: (() -> Void)?)
}

class PostureRouter: PostureRouterProtocol {

    weak var viewController: PostureViewController!

    private var imagePicker: UIImagePickerController?

    func openSettings() {
        assertionFailure()
    }

    func pickImage(with delegate: UIImagePickerControllerDelegate & UINavigationControllerDelegate, style: UIModalPresentationStyle, animated: Bool, completion: (() -> Void)?) {
        let picker = UIImagePickerController()
        picker.delegate = delegate
        picker.modalPresentationStyle = style
        imagePicker = picker
        viewController.present(picker, animated: animated, completion: completion)
    }

    func dismissImagePicker(animated: Bool, completion: (() -> Void)?) {
        guard imagePicker != nil, viewController.presentedViewController == imagePicker else {
            logger.warning("Trying to dismiss image picker when it is not presented")
            return
        }

        viewController.dismiss(animated: animated, completion: completion)
    }
}
