//
//  PostureViewController.swift
//  Spine
//
//  Created by Александр Пахомов on 08.05.2020.
//  Copyright © 2020 Александр Пахомов. All rights reserved.
//

import UIKit
import RxSwift


protocol PostureViewProtocol: class {

}

class PostureViewController: UIViewController, PostureViewProtocol {

    // MARK: - API

    var presenter: PosturePresenterProtocol!
    let configurator: PostureConfiguratorProtocol = PostureConfigurator()

    // MARK: - Private

    // MARK: Outlets

    @IBOutlet private weak var scrollView: UIScrollView!
    @IBOutlet private weak var resultTitleLabel: UILabel!
    @IBOutlet private weak var cameraCapturingView: CameraCapturingView!
    @IBOutlet private weak var photoDashboardView: UIView!
    @IBOutlet private weak var shootButton: ShootButton!
    @IBOutlet private weak var pickImageButton: UIButton!
    @IBOutlet private weak var openSettingsButton: UIButton!
    @IBOutlet private weak var resultsDashboardView: UIView!
    @IBOutlet private weak var repeatButton: UIButton!

    // MARK: Actions

    @IBAction private func pickImageButtonDidTap(_ sender: UIButton) {
        presenter.pickImageButtonDidTap()
    }

    @IBAction private func openSettingsButtonDidTap(_ sender: UIButton) {
        showNotImplementedAlert()
    }

    @IBAction private func shootButtonDidTap(_ sender: ShootButton) {
        presenter.shootButtonDidTap()
    }

    @IBAction private func repeatButtonDidTap(_ sender: UIButton) {
        presenter.repeatButtonDidTap()
    }

    @IBAction private func saveImageAsReportButtonDidTap(_ sender: UIButton) {
        showNotImplementedAlert()
    }

    // MARK: Variables

    private let bag = DisposeBag()

    // MARK: Methods

    override func viewDidLoad() {
        super.viewDidLoad()
        configurator.configure(with: self)
        presenter.configureView()

        setupObservations()
    }

    private func setupObservations() {
        presenter.oResultTitle.bind(to: resultTitleLabel.rx.text).disposed(by: bag)

        presenter.oHasActivity.bind(to: cameraCapturingView, by: \.hasFrontBlur).disposed(by: bag)
        presenter.oHasActivity.bind(to: cameraCapturingView, by: \.hasActivity).disposed(by: bag)

        presenter.oIsScrollEnabled.bind(to: scrollView.rx.isScrollEnabled).disposed(by: bag)
        presenter.oIsScrollEnabled.filter({ !$0 })
            .subscribe(onNext: { [weak self] _ in
                self?.scrollView.scrollToTop()
            }
        ).disposed(by: bag)

        presenter.oImage.bind(to: cameraCapturingView, by: \.stillImage).disposed(by: bag)

        presenter.oCameraCaptureSession.bind(to: cameraCapturingView, by: \.captureSession).disposed(by: bag)

        presenter.oIsPhotoDashboardHidden.bind(to: photoDashboardView.rx.isHidden).disposed(by: bag)
        presenter.oIsResultsDashboardHidden.bind(to: resultsDashboardView.rx.isHidden).disposed(by: bag)
    }
}
