//
//  CameraCapturingView.swift
//  Spine
//
//  Created by Александр Пахомов on 07.05.2020.
//  Copyright © 2020 Александр Пахомов. All rights reserved.
//

import UIKit
import SwiftyBeaver
import RxSwift
import AVFoundation


@IBDesignable
class CameraCapturingView: UIView {

    // MARK: - API

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    var stillImage: UIImage? {
        didSet {
            stillImageView.image = stillImage
        }
    }

    var captureSession: AVCaptureSession? {
        didSet {
            captureSessionDidChange()
        }
    }

    // MARK: - Private

    private let captureImageView = UIImageView()
    private let stillImageView = UIImageView()
    private var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    private let bag = DisposeBag()

    private func commonInit() {
        prepareForConstrain(views: captureImageView, stillImageView)
        bringSubviewToFront(stillImageView)
        addFullSizeConstrains(to: captureImageView, stillImageView)

        stillImageView.backgroundColor = .clear
    }

    private func captureSessionDidChange() {
        guard let session = captureSession else {
            videoPreviewLayer?.removeFromSuperlayer()
            videoPreviewLayer = nil
            return
        }

        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session)
        videoPreviewLayer.videoGravity = .resizeAspectFill
        videoPreviewLayer.connection?.videoOrientation = .portrait

        captureImageView.layer.addSublayer(videoPreviewLayer)
        captureImageView.layer.rx.bindBounds(toFrameOf: videoPreviewLayer).disposed(by: bag)
    }

    override func prepareForInterfaceBuilder() {
        backgroundColor = .gray
    }
}
