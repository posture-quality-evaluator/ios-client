//
//  ShootButton.swift
//  Spine
//
//  Created by Александр Пахомов on 08.05.2020.
//  Copyright © 2020 Александр Пахомов. All rights reserved.
//

import UIKit
import RxSwift
import Cartography


@IBDesignable
class ShootButton: UIButton {

    // MARK: - API

    @IBInspectable var gradientBottomColor: UIColor!
    @IBInspectable var gradientTopColor: UIColor!

    // MARK: - Private

    private let circleView = UIView()

    private let bag = DisposeBag()

    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }

    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        commonInit()
    }

    private func commonInit() {
        prepareForConstrain(views: circleView)

        constrain(circleView, self) { circleView, view in
            circleView.center == view.center
            circleView.width == circleView.height
            circleView.width == view.width - 5
        }

        circleView.backgroundColor = .clear
        circleView.layer.rx.round().disposed(by: bag)
        circleView.layer.borderWidth = 1.3
        circleView.layer.borderColor = UIColor(white: 0, alpha: 0.9).cgColor
        circleView.isUserInteractionEnabled = false

        layer.rx.round().disposed(by: bag)
        let gradient = addGradient(
            firstColor: gradientTopColor,
            secondColor: gradientBottomColor,
            startPoint: .zero,
            endPoint: CGPoint(x: 0, y: 1),
            opacity: 1
        )
        layer.rx.bindBounds(toFrameOf: gradient).disposed(by: bag)
    }
}
