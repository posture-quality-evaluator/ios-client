//
//  CATransaction+PerformWithoutAnimation.swift
//  Spine
//
//  Created by Александр Пахомов on 08.05.2020.
//  Copyright © 2020 Александр Пахомов. All rights reserved.
//

import QuartzCore

extension CATransaction {
    static func performWithoutAnimation(_ transaction: () -> Void) {
        CATransaction.begin()
        CATransaction.disableActions()
        transaction()
        CATransaction.commit()
    }
}
