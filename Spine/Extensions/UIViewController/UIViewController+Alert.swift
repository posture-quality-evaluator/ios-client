//
//  UIViewController+Alert.swift
//  Spine
//
//  Created by Александр Пахомов on 13.05.2020.
//  Copyright © 2020 Александр Пахомов. All rights reserved.
//

import UIKit


extension UIViewController {
    func showSimpleAlert(title: String, description: String, animated: Bool = true, completion: (() -> Void)? = nil) {
        let viewController = UIAlertController(title: title, message: description, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { [weak self] _ in
            self?.dismiss(animated: animated, completion: nil)
        }
        viewController.addAction(okAction)
        viewController.preferredAction = okAction

        present(viewController, animated: animated, completion: completion)
    }

    func showNotImplementedAlert(animated: Bool = true, completion: (() -> Void)? = nil) {
        showSimpleAlert(title: "Not Implemented", description: "Soon it will be done", animated: animated, completion: completion)
    }
}
