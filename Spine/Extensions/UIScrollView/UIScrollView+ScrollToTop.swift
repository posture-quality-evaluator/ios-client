//
//  UIScrollView+ScrollToTop.swift
//  Spine
//
//  Created by Александр Пахомов on 09.05.2020.
//  Copyright © 2020 Александр Пахомов. All rights reserved.
//

import UIKit


extension UIScrollView {
    func scrollToTop(animated: Bool = true) {
        setContentOffset(.zero, animated: animated)
    }
}
