//
//  PoseCNN_Multi_15+Extensions.swift
//  Spine
//
//  Created by Александр Пахомов on 10.05.2020.
//  Copyright © 2020 Александр Пахомов. All rights reserved.
//

import PostureVision
import UIKit


extension PoseCNN_Multi_15 {

    // MARK: - Base Configurations

    static let pointsDetectionConfiguration = PVPointsDetectorConfiguration(
        layersCount: layersCount,
        outputWidth: Int(outputSize.width),
        outputHeight: Int(outputSize.height),
        inputSize: inputSize,
        backgroundLayerIndex: backgroundLayerIndex
    )

    static let heatmapTransformationConfiguration = PVHeatmapToPointsTransformerConfiguration(
        layersCount: layersCount,
        outputWidth: Int(outputSize.width),
        outputHeight: Int(outputSize.height),
        points: points,
        minNmsThreshold: minNmsThreshold,
        maxNmsThreshold: maxNmsThreshold,
        nmsWindowSize: nmsWindowSize
    )

    static let pointsConnectionConfiguration = PVPointsConnectorConfiguration(
        connectionTypes: connectionTypes,
        singlePerson: singlePerson,
        pafLayerStartIndex: pafLayerStartIndex,
        interMinAboveThreshold: interMinAboveThreshold,
        interThreshold: interThreshold,
        outputSize: outputSize
    )

    static let pointsTranslatorConfiguration = PVPointsTranslatorConfiguration(
        outputSize: outputSize
    )

    static let imagePainterConfiguration = PVConnectionsPainterConfiguration(
        pointsColors: pointsColors,
        connectionsColors: connectionsColors
    )

    // MARK: - Model Description

    static let points: [PVPointType] = [
        .head, .neck,
        .rShoulder, .rElbow, .rWrist,
        .lShoulder, .lElbow, .lWrist,
        .rHip, .rKnee, .rAnkle,
        .lHip, .lKnee, .lAnkle, .chest
    ]

    static let connectionTypes: [PVConnectionType] = [
        PVConnectionType(.head, .neck, pafIndices: (0, 1)),
        PVConnectionType(.neck, .rShoulder, pafIndices: (2, 3)),
        PVConnectionType(.rShoulder, .rElbow, pafIndices: (4, 5)),
        PVConnectionType(.rElbow, .rWrist, pafIndices: (6, 7)),
        PVConnectionType(.neck, .lShoulder, pafIndices: (8, 9)),
        PVConnectionType(.lShoulder, .lElbow, pafIndices: (10, 11)),
        PVConnectionType(.lElbow, .lWrist, pafIndices: (12, 13)),
        PVConnectionType(.neck, .chest, pafIndices: (14, 15)),
        PVConnectionType(.chest, .rHip, pafIndices: (16, 17)),
        PVConnectionType(.rHip, .rKnee, pafIndices: (18, 19)),
        PVConnectionType(.rKnee, .rAnkle, pafIndices: (20, 21)),
        PVConnectionType(.chest, .lHip, pafIndices: (22, 23)),
        PVConnectionType(.lHip, .lKnee, pafIndices: (24, 25)),
        PVConnectionType(.lKnee, .lAnkle, pafIndices: (26, 27))
    ]

    private static let pointsColorLiterals = [
        0xFF0055, 0xFF0000, 0xFF5500,
        0xFFAA00, 0xFFFF00, 0xAAFF00,
        0x55FF00, 0x2BFF00, 0x00FF00,
        0x00FF55, 0x00FFAA, 0x00FFFF,
        0x00AAFF, 0x0055FF, 0xFF00AA
    ].map { UIColor(hex: $0) }

    private static let connectionsColorLiterals = [
        0xFF0055, 0xFF0000, 0xFF5500,
        0xFFAA00, 0xFFFF00, 0xAAFF00,
        0x55FF00, 0x2BFF00, 0x00FF00,
        0x00FF55, 0x00FFAA, 0x00FFFF,
        0x00AAFF, 0x0055FF
    ].map { UIColor(hex: $0) }

    static let inputSize = CGSize(width: 384, height: 384)
    static let outputSize = CGSize(width: 48, height: 48)
    static let layersCount: Int = 44
    static let heatMapLayersCount: Int = 15
    static let backgroundLayerIndex: Int = 15
    static let pafLayerStartIndex: Int = 16
    static let interMinAboveThreshold: Float32 = Float32(0.75)
    static let interThreshold: Float32 = Float32(0.01)
    static let minNmsThreshold: Float32 = Float32(0.1)
    static let maxNmsThreshold: Float32 = Float32(0.3)
    static let nmsWindowSize: Int = 7
    static let singlePerson = false

    static let pointsColors: [PVPointType: UIColor] = Dictionary(keys: points, values: pointsColorLiterals)

    static let connectionsColors: [PairKey<PVPointType>: UIColor] = Dictionary(
        keys: connectionTypes.map { PairKey<PVPointType>($0.firstPointType, $0.secondPointType) },
        values: connectionsColorLiterals
    )
}
