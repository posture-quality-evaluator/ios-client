//
//  PoseMNV2_Single_14.swift
//  Spine
//
//  Created by Александр Пахомов on 09.05.2020.
//  Copyright © 2020 Александр Пахомов. All rights reserved.
//

import PostureVision
import UIKit


extension PoseMNV2_Single_14 {

    // MARK: - Base Configurations

    static let pointsDetectionConfiguration = PVPointsDetectorConfiguration(
        layersCount: layersCount,
        outputWidth: Int(outputSize.width),
        outputHeight: Int(outputSize.height),
        inputSize: inputSize,
        backgroundLayerIndex: -1
    )

    static let heatmapTransformationConfiguration = PVHeatmapToPointsTransformerConfiguration(
        layersCount: layersCount,
        outputWidth: Int(outputSize.width),
        outputHeight: Int(outputSize.height),
        points: points,
        minNmsThreshold: minNmsThreshold,
        maxNmsThreshold: maxNmsThreshold,
        nmsWindowSize: nmsWindowSize
    )

    static let pointsConnectionConfiguration = PVPointsConnectorConfiguration(
        connectionTypes: connectionTypes,
        singlePerson: true,
        pafLayerStartIndex: -1,
        interMinAboveThreshold: 0.0,
        interThreshold: 0,
        outputSize: outputSize
    )

    static let pointsTranslatorConfiguration = PVPointsTranslatorConfiguration(
        outputSize: outputSize
    )

    static let imagePainterConfiguration = PVConnectionsPainterConfiguration(
        pointsColors: pointsColors,
        connectionsColors: connectionsColors
    )

    // MARK: - Model Description

    static let points: [PVPointType] = [
        .head, .neck,
        .rShoulder, .rElbow, .rWrist,
        .lShoulder, .lElbow, .lWrist,
        .rHip, .rKnee, .rAnkle,
        .lHip, .lKnee, .lAnkle
    ]

    static let connectionTypes: [PVConnectionType] = [
        PVConnectionType(.head, .neck),
        PVConnectionType(.neck, .rShoulder),
        PVConnectionType(.rShoulder, .rElbow),
        PVConnectionType(.rElbow, .rWrist),
        PVConnectionType(.neck, .lShoulder),
        PVConnectionType(.lShoulder, .lElbow),
        PVConnectionType(.lElbow, .lWrist),
        PVConnectionType(.neck, .rHip),
        PVConnectionType(.rHip, .rKnee),
        PVConnectionType(.rKnee, .rAnkle),
        PVConnectionType(.neck, .lHip),
        PVConnectionType(.lHip, .lKnee),
        PVConnectionType(.lKnee, .lAnkle)
    ]

    private static let pointsColorLiterals = [
        0xFF0055, 0xFF0000, 0xFF5500,
        0xFFAA00, 0xFFFF00, 0xAAFF00,
        0x55FF00, 0x2BFF00, 0x00FF00,
        0x00FF55, 0x00FFAA, 0x00FFFF,
        0x00AAFF, 0x0055FF,
    ].map { UIColor(hex: $0) }

    private static let connectionsColorLiterals = [
        0xFF0055, 0xFF0000, 0xFF5500,
        0xFFAA00, 0xFFFF00, 0xAAFF00,
        0x55FF00, 0x2BFF00, 0x00FF00,
        0x00FF55, 0x00FFAA, 0x00FFFF,
        0x00AAFF,
    ].map { UIColor(hex: $0) }

    static let inputSize = CGSize(width: 192, height: 192)
    static let outputSize = CGSize(width: 96, height: 96)
    static let layersCount: Int = 14
    static let heatMapLayersCount: Int = 14
    static let minNmsThreshold: Float32 = Float32(0.1)
    static let maxNmsThreshold: Float32 = Float32(0.3)
    static let nmsWindowSize: Int = 12

    static let pointsColors: [PVPointType: UIColor] = Dictionary(keys: points, values: pointsColorLiterals)

    static let connectionsColors: [PairKey<PVPointType>: UIColor] = Dictionary(
        keys: connectionTypes.map { PairKey<PVPointType>($0.firstPointType, $0.secondPointType) },
        values: connectionsColorLiterals
    )
}
