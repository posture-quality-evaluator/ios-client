//
//  UIColor+hex.swift
//  Spine
//
//  Created by Александр Пахомов on 09.05.2020.
//  Copyright © 2020 Александр Пахомов. All rights reserved.
//

import UIKit


extension UIColor {
    convenience init(hex: Int, alpha: Double = 1.0) {
        self.init(
            red: CGFloat((hex >> 16) & 0xFF) / 255.0,
            green: CGFloat((hex >> 8) & 0xFF) / 255.0,
            blue: CGFloat((hex) & 0xFF) / 255.0,
            alpha:  CGFloat(255 * alpha) / 255
        )
    }
}
