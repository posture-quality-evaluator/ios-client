//
//  CALayer+RoundCorners.swift
//  Spine
//
//  Created by Александр Пахомов on 07.05.2020.
//  Copyright © 2020 Александр Пахомов. All rights reserved.
//

import QuartzCore


extension CALayer {
    func roundCorners(radius: CGFloat, clipping: Bool = true) {
        cornerRadius = radius
        masksToBounds = clipping
    }
}
