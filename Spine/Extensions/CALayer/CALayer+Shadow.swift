//
//  CALayer+Shadow.swift
//  Spine
//
//  Created by Александр Пахомов on 08.05.2020.
//  Copyright © 2020 Александр Пахомов. All rights reserved.
//

import UIKit


extension CALayer {
    func setupShadow(color: CGColor, radius: CGFloat, opacity: Float, offset: CGSize) {
        shadowColor = color
        shadowOpacity = opacity
        shadowRadius = radius
        shadowOffset = offset

        masksToBounds = false
    }

    convenience init(shadowWithColor color: CGColor, blurRadius: CGFloat, opacity: Float, offset: CGSize, cornerRadius: CGFloat) {
        self.init()
        self.shadowColor = color
        self.shadowRadius = blurRadius
        self.shadowOpacity = opacity
        self.shadowOffset = offset
        self.cornerRadius = cornerRadius
    }

    static func defaultShadowLayers() -> [CALayer] {
        return [
            CALayer(shadowWithColor: UIColor.white.cgColor, blurRadius: 16, opacity: 0.15, offset: .zero, cornerRadius: 0),
            CALayer(shadowWithColor: UIColor.white.cgColor, blurRadius: 2, opacity: 0.25, offset: .zero, cornerRadius: 0),
        ]
    }
}
