//
//  UIView+FrontBlur.swift
//  Spine
//
//  Created by Александр Пахомов on 07.05.2020.
//  Copyright © 2020 Александр Пахомов. All rights reserved.
//

import UIKit


extension UIView {
    var frontBlur: UIVisualEffectView? {
        let view = subviews.first(where: { $0.tag == "front blur".hashValue && $0 is UIVisualEffectView })
        return view as? UIVisualEffectView
    }

    var hasFrontBlur: Bool {
        get {
            frontBlur != nil
        }
        set {
            guard hasFrontBlur != newValue else { return }
            if newValue {
                addFrontBlur()
            } else {
                removeFrontBlur()
            }
        }
    }

    func addFrontBlur(effect: UIVisualEffect = UIBlurEffect(style: .dark),
                      animationDuration: Double = 0.3,
                      cornerRadius: CGFloat? = nil,
                      animationDuration duration: TimeInterval = -1) {
        removeFrontBlur()
        let view = UIVisualEffectView(effect: effect)
        view.tag = "front blur".hashValue
        prepareForConstrain(views: [view])
        addFullSizeConstrains(to: view)

        UIView.performWithoutAnimation {
            layoutIfNeeded()
        }

        view.alpha = 1

        if let cornerRadius = cornerRadius {
            view.layer.roundCorners(radius: cornerRadius)
        }

        guard duration > 0 else { return }

        view.alpha = 0.0
        UIView.animate(withDuration: duration) {
            view.alpha = 1
        }
    }

    func removeFrontBlur(animationDuration duration: TimeInterval = -1) {
        guard let frontBlur = frontBlur else { return }

        guard duration > 0 else {
            frontBlur.removeFromSuperview()
            return
        }

        UIView.animate(withDuration: duration, animations: {
            frontBlur.alpha = 0.0
        }, completion: { success in
            guard success else { return }
            frontBlur.removeFromSuperview()
        })
    }
}
