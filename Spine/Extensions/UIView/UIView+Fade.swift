//
//  UIView+Fade.swift
//  Spine
//
//  Created by Александр Пахомов on 07.05.2020.
//  Copyright © 2020 Александр Пахомов. All rights reserved.
//

import UIKit


extension UIView {
    func fadeOut(with duration: TimeInterval = 0.3,
                 delay: TimeInterval = 0,
                 completion: ((UIView, Bool) -> Void)? = UIView.removeFromSuperviewIfNeeded) {
        guard alpha > 0.0 else {
            completion?(self, true)
            return
        }

        UIView.animate(withDuration: duration, delay: delay, options: [], animations: {
            self.alpha = 0
        }, completion: { success in
            completion?(self, success)
        })
    }
    func fadeIn(with duration: TimeInterval = 0.3, delay: TimeInterval = 0, startAlpha: CGFloat = 0.0) {
        alpha = startAlpha
        UIView.animate(withDuration: duration, delay: delay, options: [], animations: {
            self.alpha = 1
        })
    }

    static func removeFromSuperviewIfNeeded(_ view: UIView, needed: Bool) {
        if needed {
            view.removeFromSuperview()
        }
    }

    static func hideViewIfNeeded(_ view: UIView, needed: Bool) {
        if needed {
            view.isHidden = true
        }
    }
}
