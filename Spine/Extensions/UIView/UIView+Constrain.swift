//
//  UIView+Constrain.swift
//  Spine
//
//  Created by Александр Пахомов on 07.05.2020.
//  Copyright © 2020 Александр Пахомов. All rights reserved.
//

import UIKit
import Cartography


extension UIView {
    func prepareForConstrain(views: UIView...) {
        prepareForConstrain(views: views)
    }

    func prepareForConstrain(views: [UIView]) {
        for view in views {
            view.translatesAutoresizingMaskIntoConstraints = false
            addSubview(view)
        }
    }

    func addFullSizeConstrains(to views: UIView...) {
        for view in views {
            constrain(view, self) {
                $0.top == $1.top
                $0.bottom == $1.bottom
                $0.leading == $1.leading
                $0.trailing == $1.trailing
            }
        }
    }
}
