//
//  UIView+Shadow.swift
//  Spine
//
//  Created by Александр Пахомов on 08.05.2020.
//  Copyright © 2020 Александр Пахомов. All rights reserved.
//

import UIKit
import RxSwift


extension UIView {
    func addShadowsView() -> ShadowsView {
        let shadowsView = ShadowsView(on: self)
        return shadowsView
    }
}

class ShadowsView: UIView {
    private var shadowLayers: [CALayer] = []
    private let bag = DisposeBag()
    private let view: UIView

    init(on view: UIView) {
        self.view = view
        super.init(frame: .zero)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func commonInit() {
        backgroundColor = .clear
        isUserInteractionEnabled = false

        viewsSuperviewDidChange()

        view.rx
            .methodInvoked(#selector(didMoveToSuperview))
            .subscribe(onNext: { [weak self] _ in
                self?.viewsSuperviewDidChange()
            }
        ).disposed(by: bag)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        for layer in shadowLayers {
            layer.frame = self.layer.bounds
        }
    }

    @discardableResult
    func addShadowLayers(_ shadowLayers: [CALayer]) -> ShadowsView {
        for layer in shadowLayers {
            addShadowLayer(layer)
        }

        return self
    }

    @discardableResult
    func addShadowLayer(_ shadowLayer: CALayer) -> ShadowsView {
        shadowLayers.append(shadowLayer)
        layer.insertSublayer(shadowLayer, at: 0)
        shadowLayer.needsDisplayOnBoundsChange = true
        return self
    }

    private func viewsSuperviewDidChange() {
        removeFromSuperview()

        guard let superview = view.superview else { return }
        superview.insertSubview(self, belowSubview: view)
        view.addFullSizeConstrains(to: self)
    }
}

extension ShadowsView: Disposable {
    func dispose() {
        removeFromSuperview()
    }
}
