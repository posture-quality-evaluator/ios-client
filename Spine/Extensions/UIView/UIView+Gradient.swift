//
//  UIView+Gradient.swift
//  Spine
//
//  Created by Александр Пахомов on 08.05.2020.
//  Copyright © 2020 Александр Пахомов. All rights reserved.
//

import UIKit


extension UIView {
    func getGradientLayer(with name: String = "gradient") -> CAGradientLayer? {
        return layer.sublayers?.first { $0.name == name && $0 is CAGradientLayer } as? CAGradientLayer
    }

    @discardableResult
    func addGradient(
        firstColor: UIColor,
        secondColor: UIColor,
        startPoint: CGPoint,
        endPoint: CGPoint,
        opacity: Float,
        layerName name: String = "gradient") -> CAGradientLayer
    {
        let gradient = CAGradientLayer()
        gradient.name = name

        gradient.colors = [firstColor.cgColor, secondColor.cgColor]
        gradient.opacity = opacity

        gradient.startPoint = startPoint
        gradient.endPoint = endPoint

        gradient.bounds = bounds
        gradient.bounds.size.width = 0

        self.layer.insertSublayer(gradient, at: 0)

        return gradient
    }

    @discardableResult
    func addGrayGradient(layerName name: String = "gradient") -> CAGradientLayer {
        return addGradient(
            firstColor: Asset.Color.grayGradientTop,
            secondColor: Asset.Color.grayGradientBottom,
            startPoint: CGPoint(x: 0, y: 0),
            endPoint: CGPoint(x: 0, y: 1),
            opacity: 1,
            layerName: name
        )
    }

    func removeGradient(with name: String = "gradient") {
        getGradientLayer(with: name)?.removeFromSuperlayer()
    }

    func updateGrad(with frame: CGRect? = nil, and name: String = "gradient") {
        let frame = frame ?? bounds

        guard let gradient = getGradientLayer(with: name) else { return }

        CATransaction.performWithoutAnimation {
            gradient.frame = frame
            gradient.removeAllAnimations()
        }
    }
}
