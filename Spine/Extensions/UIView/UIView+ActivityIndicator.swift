//
//  UIView+ActivityIndicator.swift
//  Spine
//
//  Created by Александр Пахомов on 07.05.2020.
//  Copyright © 2020 Александр Пахомов. All rights reserved.
//

import UIKit
import Cartography


protocol Active: class {
    var hasActivity: Bool { get set }

    func startActivity(delay: TimeInterval)
    func stopActivity(duration: TimeInterval, delay: TimeInterval)
}

extension Active where Self: UIView {
    func startActivity(delay: TimeInterval = 0.3) {
        // stop previous activities
        self.stopActivity()

        isUserInteractionEnabled = false

        // create a new one
        let indicator = UIActivityIndicatorView(style: .large)
//        indicator.alpha = 0

        prepareForConstrain(views: indicator)
        bringSubviewToFront(indicator)
        constrain(indicator, self) {
            $0.center == $1.center
            $0.width == 100
            $0.height == $0.width
        }

        indicator.startAnimating()

        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
            indicator.fadeIn(with: 0.2, delay: 0)
        }
    }

    var activityIndicator: UIActivityIndicatorView? {
        return subviews.first(where: { $0 is UIActivityIndicatorView }) as? UIActivityIndicatorView
    }

    var hasActivity: Bool {
        get {
            activityIndicator != nil
        }
        set {
            guard hasActivity != newValue else { return }
            if newValue {
                startActivity()
            } else {
                stopActivity()
            }
        }
    }

    func stopActivity(duration: TimeInterval = 0.2, delay: TimeInterval = 0) {
        isUserInteractionEnabled = true
        activityIndicator?.fadeOut(with: duration, delay: delay)
    }
}

extension UIView: Active { }
