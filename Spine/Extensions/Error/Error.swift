//
//  Error.swift
//  Spine
//
//  Created by Александр Пахомов on 07.05.2020.
//  Copyright © 2020 Александр Пахомов. All rights reserved.
//

import Foundation


extension Error {
    var debugDescription: String {
        return (self as NSError).debugDescription
    }
}
