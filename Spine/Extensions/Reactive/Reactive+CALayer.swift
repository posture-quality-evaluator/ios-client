//
//  Reactive+CALayer.swift
//  Spine
//
//  Created by Александр Пахомов on 08.05.2020.
//  Copyright © 2020 Александр Пахомов. All rights reserved.
//

import UIKit
import RxSwift


extension Reactive where Base: CALayer {
    func round(clipping: Bool = true) -> Disposable {
        return observe(CGRect.self, #keyPath(CALayer.bounds))
            .subscribe(onNext: { _ in
                self.base.roundCorners(radius: self.base.bounds.height / 2, clipping: clipping)
            }
        )
    }

    func roundShadowPath() -> Disposable {
        return observe(CGRect.self, #keyPath(CALayer.bounds))
            .subscribe(onNext: { _ in
                let bounds = self.base.bounds
                self.base.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: bounds.height / 2).cgPath
            }
        )
    }

    func bindBounds(toFrameOf layer: CALayer) -> Disposable {
        return observe(CGRect.self, #keyPath(CALayer.bounds)).subscribe(onNext: { layer.frame = $0! })
    }
}
