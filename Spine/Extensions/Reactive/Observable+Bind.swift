//
//  Observable+Bind.swift
//  Spine
//
//  Created by Александр Пахомов on 09.05.2020.
//  Copyright © 2020 Александр Пахомов. All rights reserved.
//

import RxSwift


extension Observable {
    typealias KeyPathBindable = AnyObject

    //Check that the property is objc. Otherwise the exception will be thrown
    /// Binds observable to object's property
    func bind<O: KeyPathBindable>(to object: O, by keyPath: WritableKeyPath<O, Element>) -> Disposable {
        return subscribe(onNext: { [weak object] newValue in
            object?[keyPath: keyPath] = newValue
        })
    }
}
