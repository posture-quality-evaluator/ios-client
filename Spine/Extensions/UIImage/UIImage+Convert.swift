//
//  UIImage+Convert.swift
//  Spine
//
//  Created by Александр Пахомов on 07.05.2020.
//  Copyright © 2020 Александр Пахомов. All rights reserved.
//

import UIKit


extension UIImage {
    /// Proportion = result height / result width
    func cropAspectFit(with proportion: CGFloat) -> UIImage? {
        var newHeight = min(size.height, size.width * proportion)
        let newWidth = min(size.width, newHeight / proportion)
        newHeight = min(size.height, newWidth * proportion)

        let newOriginX = (size.width - newWidth) / 2
        let newOriginY = (size.height - newHeight) / 2
        let croppingRect = CGRect(x: newOriginX, y: newOriginY, width: newWidth, height: newHeight)

        guard let croppedCgImage = cgImage?.cropping(to: croppingRect) else { return nil }
        return UIImage(cgImage: croppedCgImage)
    }

    static func convert(from ciImage: CIImage) -> UIImage? {
        let context = CIContext(options: nil)
        guard let cgImage = context.createCGImage(ciImage, from: ciImage.extent) else { return nil }
        return UIImage(cgImage: cgImage)
    }
}
