# Posture Quality Evaluator iOS Client

The application can rate the quality of posture of a human on the photo using neural network methods.

Secondary task of the project was to try to combine VIPER architecture with binding (RxSwift). The result of such an experiment was quite positive: binding is a great tool for communication between different level objects (like View <- Presenter or Presenter <- Interactor) because now the lower layer object does not even know about upper one (like Presenter does not know about View), the only thing it has to do - is to implement provided interface with observable variables and also because now the whole state of the whole application can be stored in that variables.

The project was a course work in ITMO.

If you want to run the application you can download models from my google disk (https://drive.google.com/drive/folders/1_yomyPAhhE1W_9MTB_k3KjBDxGvW0wES?usp=sharing) or use models available at OpenPose. Models must be located at Spine/Resources/CoreMLModels
